module gitlab.com/jshsj/raw-helper

go 1.16

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/radovskyb/watcher v1.0.7
	github.com/spf13/afero v1.6.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
)
