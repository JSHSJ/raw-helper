package watcher

import (
	"fmt"
	"github.com/radovskyb/watcher"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
)

type WatcherOptions struct {
	WorkingDir    string
	RawDir        string
	RenameFiles   bool
	DeleteFiles   bool
	ImageExt      string
	RawExt        string
	Verbose       bool
	AdditionalExt []string
}

func NewWatcher(options WatcherOptions) {
	w := watcher.New()

	w.FilterOps(watcher.Rename, watcher.Remove)

	r := regexp.MustCompile(fmt.Sprintf("(?i)%s$", options.ImageExt))
	w.AddFilterHook(watcher.RegexFilterHook(r, false))

	fmt.Println(options.AdditionalExt)

	go func() {
		for {
			select {
			case event := <-w.Event:
				if event.IsDir() && event.Op == watcher.Write {
					continue
				}

				if options.RenameFiles && event.Op == watcher.Rename {
					handleRename(options, event)
				}

				if options.DeleteFiles && event.Op == watcher.Remove {
					handleDelete(options, event)
				}

			case err := <-w.Error:
				log.Fatalln(err)
			case <-w.Closed:
				return
			}
		}
	}()

	if err := w.Add(options.WorkingDir); err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Starting watcher for", options.WorkingDir)

	if err := w.Start(time.Millisecond * 100); err != nil {
		log.Fatalln(err)
	}
}

func handleRename(options WatcherOptions, event watcher.Event) {
	oldPaths := getOldFiles(event.Name(), options)
	newPaths := getNewFiles(event.Path, options)

	for index, oldFile := range oldPaths {

		_, err := os.Stat(oldFile)

		if os.IsNotExist(err) {
			log.Println("File didn't exist when trying to rename: ", oldFile)
			return
		}

		newFile := newPaths[index]

		if options.Verbose {
			log.Printf("Renaming file %s -> %s", oldFile, newFile)
		}

		err = os.Rename(oldFile, newFile)

		if err != nil {
			log.Println("Error renaming file: ", oldFile)
		}
	}

}

func handleDelete(options WatcherOptions, event watcher.Event) {
	files := getOldFiles(event.Name(), options)

	for _, file := range files {

		err := os.Remove(file)

		if options.Verbose {
			log.Printf("Deleting file %s", file)
		}

		if err != nil {
			log.Println("Error deleting file: ", file)
		}
	}
}

func getOldFiles(filename string, options WatcherOptions) []string {
	var files []string

	rawFile := options.RawDir + "/" + changeExtension(filename, options.ImageExt, options.RawExt)
	files = append(files, rawFile)

	for _, ext := range options.AdditionalExt {
		fmt.Println(ext)
		file := options.RawDir + "/" + changeExtension(filename, options.ImageExt, ext)
		files = append(files, file)
	}
	return files
}

func getNewFiles(filepath string, options WatcherOptions) []string {
	var files []string

	newName := getNewName(filepath, options.WorkingDir)
	rawFile := options.RawDir + "/" + changeExtension(newName, options.ImageExt, options.RawExt)
	files = append(files, rawFile)

	for _, ext := range options.AdditionalExt {
		file := options.RawDir + "/" + changeExtension(newName, options.ImageExt, ext)
		files = append(files, file)
	}
	return files
}

func getNewName(fullPath string, workingDir string) string {
	splitPath := strings.Split(fullPath, " -> ")
	name := strings.Replace(splitPath[1], workingDir+"/", "", 1)
	return name
}

func changeExtension(filepath string, imgExt string, ext string) string {
	r, _ := regexp.Compile(fmt.Sprintf("(?i)%s$", imgExt))
	newPath := r.ReplaceAllString(filepath, ext)
	return newPath
}
