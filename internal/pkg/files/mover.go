package files

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
)

type MoveOptions struct {
	WorkingDir string
	RawDir     string
	RawExt     string
	Verbose    bool
}

func MoveRawFiles(options MoveOptions) {
	workingDir := options.WorkingDir
	rawDir := options.RawDir

	allFiles, err := ioutil.ReadDir(workingDir)
	handleError(err, "Couldn't read dir "+workingDir)

	err = os.Mkdir(rawDir, os.ModePerm)

	rawFiles, err := findRawFiles(allFiles, options.RawExt)
	handleError(err, "Couldn't compile regex")

	for _, file := range rawFiles {
		if options.Verbose {
			log.Println("Moving: " + workingDir + "/" + file.Name() + " -> " + rawDir + "/" + file.Name())
		}
		os.Rename(workingDir+"/"+file.Name(), rawDir+"/"+file.Name())
	}
}

func handleError(err error, message string) {
	if err != nil {
		log.Fatal(message)
	}
}

func findRawFiles(files []os.FileInfo, ext string) ([]os.FileInfo, error) {
	rawRegex, err := regexp.Compile(fmt.Sprintf("(?i)%s$", ext))
	if err != nil {
		return nil, err
	}

	var rawFiles []os.FileInfo

	for _, file := range files {
		if rawRegex.MatchString(file.Name()) && !file.IsDir() {
			rawFiles = append(rawFiles, file)
		}
	}
	return rawFiles, nil
}
