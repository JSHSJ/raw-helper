package files

import (
	"github.com/spf13/afero"
	"os"
	"testing"
)

const testDir = "./test_dir/"
const shouldInclude = "should_include.raw"
const shouldInclude2 = "should_include_2.RAW"
const shouldNotInclude = "should_not_include.jpg"
const shouldNotInclude2 = "should_not_include.raw.jpg"

func TestFindRawFiles(t *testing.T) {
	var AppFs = afero.NewMemMapFs()

	AppFs.Mkdir(testDir, 0644)

	afero.WriteFile(AppFs, testDir+shouldInclude, []byte("test"), 0644)
	afero.WriteFile(AppFs, testDir+shouldInclude2, []byte("test"), 0644)
	afero.WriteFile(AppFs, testDir+shouldNotInclude, []byte("test"), 0644)
	afero.WriteFile(AppFs, testDir+shouldNotInclude2, []byte("test"), 0644)

	allFiles, e := afero.ReadDir(AppFs, testDir)
	t.Log(allFiles, e)
	rawFiles, _ := findRawFiles(allFiles)

	n := shouldInclude
	if !Contains(rawFiles, n) {
		t.Errorf("File %s not found.", n)
	}

	n = shouldInclude2
	if !Contains(rawFiles, n) {
		t.Errorf("File %s not found.", n)
	}

	n = shouldNotInclude
	if Contains(rawFiles, n) {
		t.Errorf("File %s found, but shouldn't have been.", n)
	}

	n = shouldNotInclude2
	if Contains(rawFiles, n) {
		t.Errorf("File %s found, but shouldn't have been.", n)
	}

}

func TestFileIsMoved(t *testing.T) {
	var AppFs = afero.NewOsFs()
	name := "move_me.raw"
	AppFs.MkdirAll(testDir+"raw", 0644)
	afero.WriteFile(AppFs, testDir+name, []byte("test"), 0644)

	MoveRawFiles(testDir, testDir+"raw/", false)

	fullPath := testDir + "raw/" + name

	_, err := AppFs.Stat(fullPath)

	if os.IsNotExist(err) {
		t.Errorf("file \"%s\" does not exist.\n", fullPath)
	}
}

func Contains(a []os.FileInfo, x string) bool {
	for _, n := range a {
		if x == n.Name() {
			return true
		}
	}
	return false
}
