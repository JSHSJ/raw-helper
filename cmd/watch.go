package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/jshsj/raw-helper/internal/pkg/files"
	"gitlab.com/jshsj/raw-helper/internal/pkg/watcher"
	"strings"
)

var DontMove bool

func init() {
	watchCmd.Flags().BoolVarP(&DontMove, "nomove", "m", false, "If set to true, automatically moves raw files before starting watcher.")

	rootCmd.AddCommand(watchCmd)
}

var watchCmd = &cobra.Command{
	Use:   "watch",
	Short: "Watch workingDir for file renames and deletes.",
	Long:  "Watch workingDir for file renames and deletes. Corresponding raw files are automatically renamed and deleted accordingly.",
	Run:   watch,
}

func watch(cmd *cobra.Command, args []string) {
	if !DontMove {
		moveOptions := files.MoveOptions{
			WorkingDir: workingDir,
			RawDir:     rawDir,
			RawExt:     rawExt,
			Verbose:    verbose,
		}

		files.MoveRawFiles(moveOptions)
	}

	auto_rename := viper.GetBool("raw.auto_rename")
	auto_delete := viper.GetBool("raw.auto_delete")

	imageExt := viper.GetString("base.image_ext")
	additionalExts := strings.Split(viper.GetString("raw.additional_files"), ";")

	watcherOptions := watcher.WatcherOptions{
		WorkingDir:    workingDir,
		RawDir:        rawDir,
		RenameFiles:   auto_rename,
		DeleteFiles:   auto_delete,
		ImageExt:      imageExt,
		RawExt:        rawExt,
		Verbose:       verbose,
		AdditionalExt: additionalExts,
	}

	watcher.NewWatcher(watcherOptions)
}
