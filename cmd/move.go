package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jshsj/raw-helper/internal/pkg/files"
)

func init() {
	rootCmd.AddCommand(moveCmd)
}

var moveCmd = &cobra.Command{
	Use:   "move",
	Short: "Move all raw files to their destination.",
	Long:  "Move all raw files from your workingDir to your rawDir",
	Run:   move,
}

func move(cmd *cobra.Command, args []string) {

	moveOptions := files.MoveOptions{
		WorkingDir: workingDir,
		RawDir:     rawDir,
		RawExt:     rawExt,
		Verbose:    verbose,
	}

	files.MoveRawFiles(moveOptions)
}
