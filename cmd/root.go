package cmd

import (
	"fmt"
	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var rootCmd = &cobra.Command{
	Use:   "raw",
	Short: "Small Raw helper to make handling .JPEG and .RW2 files easier",
	Long:  `A way too complicated raw helper. Moves your raw files to a directory and watches for renames and deletes.`,
}

var rawDir string
var workingDir string
var verbose bool
var rawExt string
var err error
var cfgFile string

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is ~/.config/raw/defaults.toml)")
	rootCmd.PersistentFlags().StringP("rawdir", "r", "", "raw directory (default is $WORKINGDIR/raw). Use './your-path' to assign a relative path")
	rootCmd.PersistentFlags().StringP("workingdir", "d", "", "directory used as entry to copy files from and to watch (default ist $cwd)")
	rootCmd.PersistentFlags().Bool("verbose", true, "Get more verbose logging.")

	viper.BindPFlag("raw.directory", rootCmd.PersistentFlags().Lookup("rawdir"))
	viper.BindPFlag("base.working_dir", rootCmd.PersistentFlags().Lookup("workingdir"))
	viper.BindPFlag("base.verbose", rootCmd.PersistentFlags().Lookup("verbose"))
}

func initConfig() {
	home, err := homedir.Dir()

	if err != nil {
		log.Fatal("Can't find homedir: ", err)
		os.Exit(1)
	}

	viper.AddConfigPath(filepath.Dir(home + "/.config/raw/"))
	viper.SetConfigName("defaults")
	viper.SetConfigType("toml")
	err = viper.ReadInConfig()

	if err != nil {
		log.Fatal("Can't read config: ", err)
		os.Exit(1)
	}

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
		err = viper.MergeInConfig()

		if err != nil {
			log.Fatal("Can't read config: ", err)
			os.Exit(1)
		}
	}

	if viper.GetString("base.working_dir") == "" {
		workingDir, err = os.Getwd()
		if err != nil {
			log.Fatal(err)
		}
	} else {
		workingDir = viper.GetString("base.working_dir")
	}

	rawPath := viper.GetString("raw.directory")

	if strings.HasPrefix(rawPath, "./") {
		rawDir = workingDir + rawPath[1:]
	} else {
		rawDir = rawPath
	}

	verbose = viper.GetBool("base.verbose")
	rawExt = viper.GetString("raw.extension")
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
